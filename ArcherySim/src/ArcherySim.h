#include "GL/freeglut.h"
#include "GL/gl.h"

#include <math.h>
#include <stdio.h>
#include <stdbool.h>

struct myVect {
	double i;
	double j;
	double k;
};

typedef struct myVect vector;

struct myPt {
	double i;
	double j;
	double k;
};

typedef struct myPt point;

// Draw.c
/**
 * \brief Initialises some variables and settings.
 *
 * Enables the depth buffer, sets the clear color and removes the mouse pointer
 * from view whenever it is within the bounds of the screen.
 */
void init( void );

/**
 * \brief The display function.
 *
 * Displays all the objects on the screen including the HUD, arrows, grass, the
 * sky and also the quit screen when the user chooses to do so.
 */
void display( void );

/**
 * \brief Fires the arrow.
 *
 * Provides the appropriate functions with the initial velocity and trajectory
 * of the arrow to help calculate the position of the arrow at any point in
 * time.
 *
 * \param power the energy created by the bow
 * \param xAngle the horizontal angle
 * \param yAngle the vertical angle
 */
void fireArrow(int, double, double);

/**
 * \brief Checks for collisions.
 *
 * Checks whether or not a collision occurs between the arrow and the target.
 *
 * \retval whether or not there was a collision between the arrow and the
 * target
 */
bool checkCollision();

/**
 * \brief Returns whether or not the arrow hit the target.
 *
 * Returns whether or not the last arrow fired hit the target or not.
 *
 * \retval whether or not the arrow hit the target
 */
bool getHit();

/**
 * \brief Toggles the quit screen on.
 *
 * Toggles whether or not the quit screen will display the quit screen the next
 * frame.
 */
void toggleQuitting();

/**
 * \brief Gets the arrows position.
 *
 * Gets the position of the arrow at the present frame.
 *
 * \retval the position of the arrow
 */
vector getArrowPosition();

/**
 * \brief Reshapes the window.
 *
 * Handles the reshaping of the window to ensure the windows viewport is of the
 * correct size.
 *
 * \param w the width of the screen
 * \param h the height of the screeen
 */
void reshape(GLsizei, GLsizei);

/**
 * \brief Gets the width.
 *
 * Gets the width of the screen.
 *
 * \retval the width of the screen
 */
int getWidth();

/**
 * \brief Gets the height.
 *
 * Gets the height of the screen.
 *
 * \retval the height of the screen
 */
int getHeight();

// InputHandler.c
/**
 * \brief The mouse handler.
 *
 * Handles the operations produced by clicking the mouse.
 *
 * \param button the mouse button that was pressed
 * \param state the state of that mouse button
 * \param x the x coordinate of the cursor
 * \param y the y coordinate of the cursor
 */
void myMouse(int , int , int , int );

/**
 * \brief The keyboard handler.
 *
 * Handles the operations produced by pressed buttons on the keyboard.
 *
 * \param key the key that was pressed
 * \param x the x coordinate of the mouse at this point
 * \param y the y coordinate of the mouse at this point
 */
void myKeyboard(unsigned char , int , int );

/**
 * \brief Gets the distance to the target.
 *
 * Gets the distance from the origin to the target along the z-axis.
 *
 * \retval the distance to the target
 */
float getTargetDistance();

/**
 * \brief Rotates the world.
 *
 * Rotates the world based upon how much the camera should be rotated.
 */
void camera( void );

/**
 * \brief Returns whether or not to quit.
 *
 * Returns whether or not the escape button was pressed to cause the program to
 * exit.
 *
 * \retval whether or not to quit
 */
bool getQuitting( void );

/**
 * \brief Ambient mouse movement.
 *
 * Performs operations based upon where the mouse pointer was moved when no
 * buttons have been pressed.
 *
 * \param x the x coordinate of the mouse pointer
 * \param y the y coordinate of the mouse pointer
 */
void mouseMovement(int, int);

/**
 * \brief Mouse movement when a button has been pressed.
 *
 * Performs operations based upon where the mouse pointer is moved when a
 * button has been pressed.
 *
 * \param x the x coordinate of the mouse pointer
 * \param y the y coordinate of the mouse pointer
 */
void pullBackMovement(int, int);

/**
 * \brief Gets the rotation of the camera.
 *
 * Gets the horizontal rotation of the camera.
 *
 * \retval the horizontal rotation of the camera
 */
double getCameraRotationX();

/**
 * \brief Gets the rotation of the camera.
 *
 * Gets the vertical rotation of the camera.
 *
 * \retval the vertical rotation of the camera
 */
double getCameraRotationY();

/**
 * \brief Restricts the value of a variable
 *
 * Restricts the variable to within the bounds of the other parameters.
 *
 * \param min the minimum bound
 * \param max the maximum bound
 * \param val the variable
 * \retval the value of the variable, if it is within the bounds, the variable
 * is returned however if it is outside of the bound, then that respective bound
 * is returned.
 */
double clamp(double, double, double);

// ProjectileMotion.c
/**
 * \brief Calculates the initial velocity.
 *
 * Given the velocity, vertical angle and horizontal angle, this function works
 * out the velocity components in the x, y, and z directions.
 *
 * \param velocity the velocity of the arrow
 * \param verticalAngle the vertical angle
 * \param horizontalAngle the horizontal angle
 */
void calculateInitialVelocity(double, double, double);

/**
 * \brief Gets the position of the arrow.
 *
 * Returns the position of the arrow at a given point in time.
 *
 * \param time the time
 * \retval the position of the arrow
 */
vector getPosition(double);

/**
 * \brief Gets the vertical rotation of the arrow.
 *
 * Gets the vertical rotation of the arrow around its midpoint at a patricular
 * point in time.
 *
 * \param time the time
 * \retval the rotation of the arrow
 */
double getVerticalRotation(double);

/**
 * \brief Gets the horizontal rotation of the arrow.
 *
 * Gets the horizontal rotation of the arrow around its midpoint at any point in
 * time.
 *
 * \retval the rotation of the arrow
 */
double getHorizontalRotation();

/**
 * \brief Converts degrees to radians.
 *
 * Converts degrees to radians
 *
 * \param degrees the input degrees
 * \retval the output radians
 */
double degreesToRadians(double);

// BowPhysics.c
/**
 * \brief Sets the draw weight.
 *
 * Sets the draw weight of the bow, the amount of force required to pull the
 * bowstring to its maximum draw length
 *
 * \param drawWeight the draw weight
 */
void setDrawWeight(double);

/**
 * \brief Sets the draw length.
 *
 * Sets the draw length of the bow, the maximum displacement of the bow string
 * from its original position.
 *
 * \param drawLength the draw length
 */
void setDrawLength(double);

/**
 * \brief Gets the draw length.
 *
 * Gets the draw length of the bow.
 *
 * \retval the draw length
 */
double getDrawLength();

/**
 * \brief Gets the draw weight.
 *
 * Gets the draw weight of the bow.
 *
 * \retval the draw weight
 */
double getDrawWeight();

/**
 * \brief Calculates the initial velocity.
 *
 * Calculates the initial velocity of the arrow given the input draw length and
 * draw weight.
 *
 * \retval the initial velocity of the arrow
 */
double getInitialVelocity();

// Objects.c
/**
 * \brief Defines the floor.
 *
 * Defines the floor of the world given the size coordinates.
 *
 * \param lft the left side of the floor
 * \param rgh the right side of the floor
 * \param nr the near side of the floor
 * \param fr the far side of the floor
 */
void setting(double, double, double, double);

/**
 * \brief Draws the arrow.
 *
 * Draws the arrow.
 */
void arrow( void );

/**
 * \brief Draws the arrow.
 *
 * Draws the arrow.
 */
void target( void );

/**
 * \brief Draws the HUD.
 *
 * Draws the HUD.
 */
void drawHUD( void );

/**
 * \brief Draws the crosshair.
 *
 * Draws the crosshair.
 */
void crosshair( void );

/**
 * \brief Draws the controls.
 *
 * Draws the controls.
 */
void writeControls( void );

/**
 * \brief Draws the variables.
 *
 * Draws the variables.
 */
void writeVariables( void );

/**
 * \brief Draws the quit screen.
 *
 * Draws the quit screen.
 */
void quitScreen( void );

// Texture.c
/**
 * \brief Loads the texture.
 *
 * Loads the input texture given the filename, width and height.
 *
 * \param filename the file of the texture
 * \param width the width of the texture
 * \param height the height of the texture
 * \retval the texture
 */
GLuint LoadTexture( const char * filename, int width, int height);

/**
 * \brief Frees the texture memory.
 *
 * Frees the texture memory.
 *
 * \param the texture to be freed
 */
void FreeTexture( GLuint texture );
