#include "ArcherySim.h"

//------------------------------------------------------------------------------

float xPosition = 0, yPosition = 0, xRotation = 0, yRotation = 0;

float lastX, lastY;

float targetDistance = 50.0;
double drawWeight;
double drawLength;

bool pressed = false;

int timePressed = 0;
int pressedX = 0;
int pressedY = 0;

float yPullBack = 0;

bool quitting = false;

//------------------------------------------------------------------------------

void myMouse(int button, int state, int x, int y)
{
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && !pressed && !quitting)
	{
		pressed = true;
		timePressed = glutGet(GLUT_ELAPSED_TIME);
		pressedX = x;
		pressedY = y;
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		pressed = false;
		fireArrow(yPullBack, yRotation, xRotation);
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && quitting)
	{
		exit(0);
	}
}

//------------------------------------------------------------------------------

void myKeyboard(unsigned char key, int x, int y)
{
	drawWeight = getDrawWeight();
	drawLength = getDrawLength();

	switch(key) {
		case 'q':
					targetDistance += 10.0;
					break;
		case 'a':
					targetDistance -= 10.0;
					break;
		case 'w':
					drawWeight += 10.0;
					break;
		case 's':
					drawWeight -= 10.0;
					break;
		case 'e':
					drawLength += 10.0;
					break;
		case 'd':
					drawLength -= 10.0;
					break;
		case 27:
					// draw photo
					quitting = true;
					toggleQuitting();
					quitScreen();
					break;
	}

	targetDistance = clamp(10.0, 100.0, targetDistance);
	drawWeight = clamp(150.0, 300.0, drawWeight);
	drawLength = clamp(50.0, 150.0, drawLength);

	setDrawWeight(drawWeight);
	setDrawLength(drawLength);

	glutPostRedisplay();
}

float getTargetDistance() {
	return targetDistance;
}

//------------------------------------------------------------------------------

void camera( void )
{
	glRotatef(xRotation, 1.0, 0.0, 0.0); // rotate our camera around the x-axis
	glRotatef(yRotation, 0.0, 1.0, 0.0); // rotate our camera around the y-axis
}

bool getQuitting( void )
{
	return quitting;
}

//------------------------------------------------------------------------------

void mouseMovement(int x, int y) {
	float width = (float)getWidth();
	float height = (float)getHeight();
	int diffX = x - lastX;
	int diffY = y - lastY;

	lastX = x;
	lastY = y;

	xPosition += (float) diffX;
	yPosition += (float) diffY;

	yRotation = 60 * ((xPosition - (width / 2.0)) / width);
	xRotation = -(50 - (60 * (clamp(height / 2.0, height, yPosition) / height)));

	glutPostRedisplay();
}

void pullBackMovement(int x, int y) {
	float width = (float)getWidth();
	int diffX = x - lastX;
	int diffY = y - lastY;

	lastX = x;
	lastY = y;

	xPosition += (float) diffX;
	yPosition += (float) diffY;

	yRotation = 60 * ((xPosition - (width / 2.0)) / width);

	yPullBack = clamp(5, 100, -(pressedY - yPosition));

	glutPostRedisplay();
}

double getCameraRotationX() {
    return xRotation;
}

double getCameraRotationY() {
    return yRotation;
}

double clamp(double min, double max, double val) {
	if(val < min) {
		return min;
	} else if (val > max) {
		return max;
	} else {
		return val;
	}
}

