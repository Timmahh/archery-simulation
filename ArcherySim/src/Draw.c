#include "ArcherySim.h"

//------------------------------------------------------------------------------

int WIDTH = 800;
int HEIGHT = 600;

int timeSinceShot  = 0;
bool fired = false;
vector arrowPosition;

bool hitTarget = false;
bool quit = false;

//------------------------------------------------------------------------------

void init( void ) {
    // Hide the mouse cursor
    glutSetCursor(GLUT_CURSOR_NONE);

    glClearColor(0.6, 0.85, 0.92, 1.0);

    glEnable(GL_DEPTH_TEST);
}

//------------------------------------------------------------------------------

void display( void ) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    if(!quit) {
    camera();

	setting(-25.0, 25.0, -25.0, 100.0);

    glPushMatrix();
        glTranslatef(0.0, 0.0, -getTargetDistance());
        target();
    glPopMatrix();

	double arrowVerticalRotation;
	double arrowHorizontalRotation;

	if(fired)
	{
		double time = ((double)glutGet(GLUT_ELAPSED_TIME) - (double)timeSinceShot) / 1000.0;

		arrowPosition = getPosition(time);
		arrowVerticalRotation = getVerticalRotation(time);
		arrowHorizontalRotation = getHorizontalRotation();

		if(arrowPosition.j < -1.0)
		{
			fired = false;
		}
	}
	else
	{
		arrowVerticalRotation = 0;
		arrowHorizontalRotation = 0;

		arrowPosition.i = 0;
		arrowPosition.j = 0;
		arrowPosition.k = 0;
	}

	if(checkCollision() && !hitTarget)
	{
		hitTarget = true;
		fired = false;
	}

    glPushMatrix();
        glTranslatef(arrowPosition.i, arrowPosition.j, -arrowPosition.k);
        glRotatef(arrowVerticalRotation, 1.0, 0.0, 0.0);
        glRotatef(180 - arrowHorizontalRotation, 0.0, 1.0, 0.0);
        arrow();
    glPopMatrix();

    drawHUD();
    }
    else
    {
    	quitScreen();
    }

    glFlush();
    glutSwapBuffers();
}

//------------------------------------------------------------------------------

void fireArrow(int power, double xAngle, double yAngle)
{
	hitTarget = false;

	double drawLength = (power * getDrawLength()) / 100.0;

	double temp = getDrawLength();

	setDrawLength(drawLength);

	double velocity = getInitialVelocity();

	calculateInitialVelocity(velocity, -yAngle, xAngle);

	timeSinceShot = glutGet(GLUT_ELAPSED_TIME);

	fired = true;

	setDrawLength(temp);
}

void toggleQuitting()
{
	quit = true;
}

bool checkCollision()
{
	bool collision = false;

	double temp = arrowPosition.k - getTargetDistance();
	if((arrowPosition.i < 1.5 && arrowPosition.i > -1.5) &&
		(arrowPosition.j < 3.0 && arrowPosition.j > 0.0) &&
		(temp < 1.0 && temp > -1.0))
	{
		collision = true;
	}

	return collision;
}

bool getHit()
{
	return hitTarget;
}

vector getArrowPosition()
{
	return arrowPosition;
}

//------------------------------------------------------------------------------

void reshape(GLsizei w, GLsizei h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 1000.0);
    glMatrixMode(GL_MODELVIEW);

    WIDTH = w;
    HEIGHT = h;
}

int getWidth() {
    return WIDTH;
}

int getHeight() {
    return HEIGHT;
    }

//------------------------------------------------------------------------------

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("Archery Simulation");

    init();

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMouseFunc(myMouse);
    glutKeyboardFunc(myKeyboard);
    glutIdleFunc(display);
    glutPassiveMotionFunc(mouseMovement);
    glutMotionFunc(pullBackMovement);

    glutMainLoop();

    return 0;
    }
