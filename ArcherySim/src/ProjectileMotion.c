#include "ArcherySim.h"

//------------------------------------------------------------------------------

const double GRAVITY = 10.0;

double yAngle = 15.0;
double xAngle = 0.0;

vector initialVelocity;

//------------------------------------------------------------------------------

void calculateInitialVelocity(double velocity, double verticalAngle,
							double horizontalAngle) {
	double vert = degreesToRadians(verticalAngle);
	double hori = degreesToRadians(horizontalAngle);

	yAngle = verticalAngle;
	xAngle = horizontalAngle;

	initialVelocity.k = (velocity / 2) * (cos(vert - hori) +
										cos(vert + hori));
	initialVelocity.j = velocity * sin(vert);
	initialVelocity.i = (velocity / 2) * (sin(vert + hori) -
										sin(vert - hori));
}

vector getPosition(double time) {
	vector position;

	position.i = initialVelocity.i * time;
	position.j = initialVelocity.j * time + (-GRAVITY / 2.0) * pow(time ,2);
	position.k = initialVelocity.k * time;

	return position;
}

double getVerticalRotation(double time) {
	return (yAngle - (GRAVITY * time));
}

double getHorizontalRotation() {
	return xAngle;
}

double degreesToRadians(double degrees) {
	double conversion = 3.14159265358979323846 / 180.0;

	return (degrees * conversion);
}
