#include "ArcherySim.h"

//------------------------------------------------------------------------------

const double MASS_OF_BOW = 20.0;
double draw_weight = 220.0;
double draw_length = 90.0;

//------------------------------------------------------------------------------

void setDrawWeight(double drawWeight) {
	draw_weight = drawWeight;
}

void setDrawLength(double drawLength) {
	draw_length = drawLength;
}

double getDrawLength()
{
	return draw_length;
}

double getDrawWeight()
{
	return draw_weight;
}

double getInitialVelocity() {
	double initVel = sqrt((draw_weight * draw_length) / MASS_OF_BOW);

	return initVel;
}

