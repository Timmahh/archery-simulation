#include "ArcherySim.h"

GLuint texture;

#define _maxRow  360
#define _maxCol  360

typedef unsigned char pixel;
typedef char name[15];

name image_file_name;

/* image buffers used for display */
pixel image_buf[_maxRow * _maxCol], out_image_buf[_maxRow * _maxCol];
/* image arrays */
pixel image[_maxRow][_maxCol], out_image[_maxRow][_maxCol];
pixel p;

char * inf_name, outf_name;
FILE *inf_handle, *outf_handle;

int charin;
int r, c;

//------------------------------------------------------------------------------

void setting(double lft, double rgh, double nr, double fr) {
    int i;

    glColor3b(0, 77, 0);

    for (i = -25; i <= 25; i += 1.0) {
        glBegin(GL_QUADS);
        glVertex3f(i, -1.0, nr);
        glVertex3f(i, -1.0, fr);
        glVertex3f(lft, -1.0, i);
        glVertex3f(rgh, -1.0, i);
        glEnd();
        }

    }

//------------------------------------------------------------------------------

void arrow( void ) {
    glPushMatrix();
    glColor3f(0.0, 0.0, 0.0);
    GLUquadricObj *quadObj = gluNewQuadric();
    gluCylinder(quadObj, 0.03, 0.0, 0.1, 40, 40);
    glTranslatef(0.0, 0.0, -1.0);
    gluCylinder(quadObj, 0.03, 0.03, 1.0, 40, 40);

    glBegin(GL_POLYGON);
    glVertex3f(0.0, 0.1, 0.0);
    glVertex3f(0.0, -0.1, 0.0);
    glVertex3f(0.0, 0.0, 0.1);
    glEnd();
    glPopMatrix();
    }

//------------------------------------------------------------------------------

void target( void ) {
    glColor3f(1.0, 1.0, 1.0);
    glEnable( GL_TEXTURE_2D );

    texture = LoadTexture("texture.raw", 256, 256);

    glBindTexture(GL_TEXTURE_2D, texture);

    glBegin(GL_QUADS);
    glTexCoord2d(0.0,0.0);
    glVertex3f(-1.5, 0.0, 0.0);
    glTexCoord2d(1.0,0.0);
    glVertex3f(1.5, 0.0, 0.0);
    glTexCoord2d(1.0,1.0);
    glVertex3f(1.5, 3.0, 0.0);
    glTexCoord2d(0.0,1.0);
    glVertex3f(-1.5, 3.0, 0.0);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    FreeTexture(texture);
}

//------------------------------------------------------------------------------

void drawHUD( void ) {
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0.0, getWidth(), getHeight(), 0.0, -1.0, 10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClear(GL_DEPTH_BUFFER_BIT);

    // draw HUD elements
    if(!getQuitting())
    {
		writeControls();
		writeVariables();
		crosshair();
    }

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    }

void crosshair( void ) {
    int size = 20;
    int width = getWidth();
    int height = getHeight();

    glLineWidth(2.0f);

    glBegin(GL_LINES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(width / 2.0 - size, height / 2.0, -1.0);
    glVertex3f(width / 2.0 + size, height / 2.0, -1.0);
    glVertex3f(width / 2.0, height / 2.0 - size, -1.0);
    glVertex3f(width / 2.0, height / 2.0 + size, -1.0);
    glEnd();
    }

void writeControls( void ) {
    int i;
    char controls[] = "CONTROLS";
    char target[]  = "Target Distance Increase/Decrease  Q/A";
    char dWeight[] = "Draw Weight Increase/Decrease      W/S";
    char dLength[] = "Draw Length Increase/Decrease      E/D";
    char aiming[]  = "Aim using the mouse.";
    char fire01[]  = "Click and pull down to draw back your";
    char fire02[]  = "bow, then release to fire.";
    char quit[]    = "To Quit                                           ESC";

    char hit[] = "HIT";

    glColor3f(0.0, 0.0, 0.0);

    glRasterPos2i(getWidth() - 150, 20);
    for(i = 0; i < sizeof(controls); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, controls[i]);
        }

    glRasterPos2i(getWidth() - 200, 40);
    for(i = 0; i < sizeof(target); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, target[i]);
        }

    glRasterPos2i(getWidth() - 200, 60);
    for(i = 0; i < sizeof(dWeight); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, dWeight[i]);
        }

    glRasterPos2i(getWidth() - 200, 80);
    for(i = 0; i < sizeof(dLength); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, dLength[i]);
        }

    glRasterPos2i(getWidth() - 200, 100);
    for(i = 0; i < sizeof(aiming); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, aiming[i]);
        }

    glRasterPos2i(getWidth() - 200, 120);
    for(i = 0; i < sizeof(fire01); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, fire01[i]);
        }

    glRasterPos2i(getWidth() - 200, 140);
    for(i = 0; i < sizeof(fire02); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, fire02[i]);
        }

	glRasterPos2i(getWidth() - 200, 160);
    for(i = 0; i < sizeof(quit); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, quit[i]);
        }

    glRasterPos2i((getWidth() / 2) - 20, 140);
    if(getHit()) {
        for(i = 0; i < sizeof(hit); i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, hit[i]);
		}
	}
}

void writeVariables( void ) {
    int i;
    vector arrowPosition = getArrowPosition();

    char distance[] = "                     ";
    sprintf(distance, "Target Distance = %dm", (int)getTargetDistance());
    char dWeight[19];
    sprintf(dWeight, "Draw Weight = %dN", (int)getDrawWeight());
    char dLength[20];
    sprintf(dLength, "Draw Length = %.2fm", getDrawLength() / 100.0);
    char pos[] = "                        ";
    sprintf(pos, "Arrow Distance = %2.2fm", arrowPosition.k);

    glColor3f(0.0, 0.0, 0.0);

    glRasterPos2i(20, 20);
    for(i = 0; i < sizeof(distance); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, distance[i]);
        }

    glRasterPos2i(20, 40);
    for(i = 0; i < sizeof(dWeight); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, dWeight[i]);
        }

    glRasterPos2i(20, 60);
    for(i = 0; i < sizeof(dLength); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, dLength[i]);
        }
    glRasterPos2i(20, 80);
    for(i = 0; i < sizeof(pos); i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, pos[i]);
        }
}

void quitScreen( void )
{
	inf_name = "image.raw";

	if((inf_handle = fopen(inf_name, "rb")) == NULL)
	{
		puts("*** Can't open input file - please check file name typed!\n");
		charin = getchar();
		exit(1);
	}

	for(r = 0; r < _maxRow; r++)
	{
		for(c = 0; c < _maxCol; c++)
		{
			if((charin=fgetc(inf_handle)) == EOF)
			{
				printf("** File reading failed! ***\n");
				charin = getchar();
				exit(1);
			}

			image[r][c] = charin;
		}
	}

	fclose(inf_handle);

	int offset;

	offset = 0;

	for(r = _maxRow - 1; r >= 0; r--)
	{
		for(c = 0; c < _maxCol; c++)
		{
			image_buf[_maxCol * offset + c] = image[r][c];
		}
		offset++;
	}

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(0.0, getWidth(), 0.0, getHeight());

	glMatrixMode(GL_MODELVIEW);

	glRasterPos2i((getWidth() - 360) / 2, (getHeight() - 360) / 2);
	glDrawPixels(_maxCol, _maxRow, GL_LUMINANCE, GL_UNSIGNED_BYTE, image_buf);

	int i;

	char madeby[] = "Made by Timothy Veletta";

	glColor3f(1.0, 1.0, 1.0);

    glRasterPos2i((getWidth() - 207) / 2, (getHeight() - 400) / 2);
    for(i = 0; i < sizeof(madeby); i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, madeby[i]);
	}
}
